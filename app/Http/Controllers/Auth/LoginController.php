<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        if ($request->isMethod('POST')) {
            if (!empty($request->email) && !empty($request->password)) {
                $user = User::where('email', $request->email)->first();
                if (!empty($user)) {
                    if (\Hash::check($request->password, $user->password)) {
                        Auth::loginUsingId($user->id);
                        $request->session()->regenerate();
                        return redirect()->route('dashboard');
                    } else {
                        Session::flash('error', 'Password is invalid.');
                        return redirect()->route('login');
                    }
                } else {
                    Session::flash('error', 'No such registered user found. Please try again.');
                    return redirect()->route('login');
                }
            } else {
                Session::flash('error', 'Please provide email and password.');
                return redirect()->route('login');
            }
        } else {
            return view('auth.login');
        }
    }
    /*
    * Signout from system
    */
    public function logout()
    {
        Auth::logout();
        Session::flash('success', 'You are successfully logged out.');
        return redirect('admin');
    }
}
