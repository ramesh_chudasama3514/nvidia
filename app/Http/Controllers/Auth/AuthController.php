<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use MustVerifyEmail;
use Validator;
use Illuminate\Validation\Rule;
use Session;
class AuthController extends Controller
{
    

    public function storeUser(Request $request)
    {
	        $request->validate([
	            'firstname' => 'required|string|max:255',
	            'lastname' => 'required|string|max:255',
	            'email' => 'required|string|email|max:255|unique:users',
	            'contact_no' => 'required|integer',
	            'password' => 'required|string|min:8|confirmed',
	            'password_confirmation' => 'required',
	        ]);
        try{
	        $user = User::create([
	            'firstname' => $request->firstname,
	            'lastname' => $request->lastname,
	            'email' => $request->email,
	            'contact_no' => $request->contact_no,
	            'password' => Hash::make($request->password),
	        ]);
            $user->sendEmailVerificationNotification();
            Session::flash('success', 'Registration completed successfully. Check your mail to confirm email.');
        	return redirect('/');
    	}catch(\Exception $e){
            Session::flash('error', $e->getMessage());
    		return redirect('/');
    	}
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user= Auth::user();
            if($user->email_verified_at != ""){
                return redirect()->intended('/')->with('success', 'Your logged in successfully!');
            }else{
                Auth::logout();
                 return redirect()->intended('/')->with('error', 'Please confirm your email');
            }
        }
        Session::flash('error', 'Oppes! You have entered invalid credentials');
        return redirect('/');
    }

    public function logout() {
      Auth::logout();
      Session::flash('success', 'Your logged out successfully!');
      return redirect('/')->with('success', 'Your logged out successfully!');
    }

    public function home()
    {
      return view('/');
    }

    /**
     * Change users password
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request)
    {
        if(Auth::Check())
        {
            $requestData = $request->All();
            $validator = $this->validatePasswords($requestData);
            if($validator->fails())
            {
                return back()->withErrors($validator->getMessageBag());
            }
            else
            {
                $currentPassword = Auth::User()->password;
                if(Hash::check($requestData['password'], $currentPassword))
                {
                    $userId = Auth::User()->id;
                    $user = User::find($userId);
                    $user->password = Hash::make($requestData['new-password']);;
                    $user->save();
                    Session::flash('success', 'Your password has been updated successfully.');
                    return back();
                }
                else
                {
                    Session::flash('error', 'Sorry, your current password was not recognised. Please try again');
                    return back();
                }
            }
        }
        else
        {
            // Auth check failed - redirect to domain root
            return redirect()->to('/');
        }
    }

    /**
     * Validate password entry
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatePasswords(array $data)
    {
        $messages = [
            'password.required' => 'Please enter your current password',
            'new-password.required' => 'Please enter a new password',
            'new-password-confirmation.not_in' => 'Sorry, common passwords are not allowed. Please try a different new password.'
        ];

        $validator = Validator::make($data, [
            'password' => 'required',
            'new-password' => ['required', 'same:new-password', 'min:8', Rule::notIn($this->bannedPasswords())],
            'new-password-confirmation' => 'required|same:new-password',
        ], $messages);

        return $validator;
    }

    /**
     * Get an array of all common passwords which we don't allow
     *
     * @return array
     */
    public function bannedPasswords(){
        return [
            'password', '12345678', '123456789', 'baseball', 'football', 'jennifer', '11111111', '222222222', '33333333', 'qwerty123'
        ];
    }
}
