<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\View;
use App\User;
use App\Upload;
use Session;
use Storage;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    public function getProfileDetails()
    { 
        $users = Auth::user();
        $uploads = Upload::where('user_id', $users->id)->first();
        return view('auth.profile', ['user' => $users,'uploads' => $uploads]);
    }
    public function geteditDetailsPage()
    {
         $users = Auth::user();
        $uploads = Upload::where('user_id', $users->id)->first();
        return view('auth.editprofile', ['user' => $users,'uploads' => $uploads]);
    }
    public function updateProfileDetails(Request $request)
    {
    	$request->validate([
	            'firstname' => 'required|string|max:255',
	            'lastname' => 'required|string|max:255',
	            'contact_no' => 'required|integer',
                'resume' => 'required||mimes:docx,pdf'
	        ]);
    	try{
            $profileName = isset($request->hidden_profile_pic) ? $request->hidden_profile_pic : $request->profile_pic;
    		$userId = Auth::id();
    		$user = User::findOrFail($userId);
            /** Resume */
                if(request()->resume != null){
                    $resume = $user->id.'_resume'.rand(0,1000).'.'.request()->resume->getClientOriginalExtension();
                    // $request->resume->storeAs('resume',$resume);   
                    Storage::disk('s3')->put($resume, file_get_contents($request->resume));
                    $user->resume = $resume;
                }
            if(isset($request->update_profile_pic) && $request->update_profile_pic != ""){
                $profileName = $user->id.'_avatar'.time().'.'.request()->update_profile_pic->getClientOriginalExtension();
                // $request->update_profile_pic->storeAs('avatars',$profileName);
                Storage::disk('s3')->put($profileName, file_get_contents($request->update_profile_pic));
            }elseif(!isset($request->hidden_profile_pic) && $request->profile_pic){
               
                // $this->validate($request,[
                //     'profile_pic' =>  'mimes:png',
                // ]);
                $profileName = $user->id.'_avatar'.time().'.'.request()->profile_pic->getClientOriginalExtension();
                // $request->profile_pic->storeAs('avatars',$profileName);
                Storage::disk('s3')->put($profileName, file_get_contents($request->profile_pic));
            }
           // if(isset($request->interested_to_register)){
           //      $interested = 1;
           //  }else{
           //      $interested = NULL;
           //  }
           //  if(isset($request->startup)){
           //      $startup = 1;
           //  }else{
           //      $startup = NULL;
           //  }
    		 $user->fill([
	            'contact_no' => $request->contact_no,
                'dob' => $request->dob,
                'address' => $request->address,
                'city' => $request->city,
                'state' => $request->state,
                'pincode' => $request->pincode,
                'education' => $request->education,
                'school_clg' => $request->school_clg,
                'profile_pic' => $profileName,
                'startup' => $request->startup,
                'interested_to_register' => $request->interested_to_register,
                'company_name' => $request->company_name
	        ]);
	        // Save user to database
            Session::flash('success', 'Profile updated successfully.');// 
	        $user->save();
            return redirect('user/myprofile');
	    }catch(\Exception $e){
            Session::flash('error', $e->getMessage());
    		 return redirect('user/myprofile');
    	}
    }

    public function getUploadDetails()
    {
        $userId = Auth::id();
        $uploads = Upload::where('user_id', $userId)->first();
        return view('cerexam.my-uploads', ['data' => $uploads]);
        
    }

    public function updateUploadDetails(Request $request)
    { 
        $request->validate([
                'name' => 'required|string|max:255',
                'devkit' => 'required|string|max:255',
            ]);
        try{
            $userId = Auth::id();
            $uploads = Upload::where('user_id', $userId)->first();

            if($uploads){
                $uploads->fill([
                    'user_id' => $userId,
                    'name' => $request->name,
                    'devkit' => $request->devkit,
                    'other_accessories_used' => $request->other_accessories_used,
                    'usecase' => $request->usecase,
                    'otherdetails' => $request->otherdetails
                ]);
            }else{
                $uploads = new Upload;
                $uploads->user_id = $userId;
                $uploads->name = $request->name;
                $uploads->devkit = $request->devkit;
                $uploads->other_accessories_used = $request->other_accessories_used;
                $uploads->usecase = $request->usecase;
                $uploads->otherdetails = $request->otherdetails;
            }
            // Save uploads to database
            $uploads->save();
            Session::flash('success', 'Details updated successfully.');
            return redirect('user/my-document');
        }catch(\Exception $e){
            Session::flash('error', $e->getMessage());
            return redirect('user/my-uploads');
        }
    }
    /**
     * Get My Documents
     */
    public function getMyDocuments(Request $request){
        $userId = Auth::id();
        $uploads = Upload::where('user_id', $userId)->first();
        return view('cerexam.document', ['data' => $uploads]);
    }
    /**
     * Update Documents
     */
    public function updateDocuments(Request $request){
        $userId = Auth::id();
        $uploads = Upload::where('user_id', $userId)->first();
        if($uploads->document_file != null){
            $request->validate([
            'document_file' => 'mimes:docx,pdf',
            'picture_1' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'picture_2' => 'image|mimes:jpeg,png,jpg|max:2048',
            'picture_3' => 'image|mimes:jpeg,png,jpg|max:2048',
            'video' => 'required|mimes:mp4,mov|max:20000'
        ]);
        }else{
            $request->validate([
                'document_file' => 'required|mimes:docx,pdf',
                'picture_1' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                'picture_2' => 'image|mimes:jpeg,png,jpg|max:2048',
                'picture_3' => 'image|mimes:jpeg,png,jpg|max:2048',
                'video' => 'required|mimes:mp4,mov|max:20000'
            ]);
         }
        try{
            $data = $request->all();
            unset($data['_token']);
            if(!empty($data) && $uploads){
                /** Document File */
                if(request()->document_file != null){
                    $documentFile = $uploads->id.'_document'.rand(0,1000).'.'.request()->document_file->getClientOriginalExtension();
                    Storage::disk('s3')->put($documentFile, file_get_contents($request->document_file));
                    // $request->document_file->storeAs('documents',$documentFile);   
                    $uploads->document_file = $documentFile;
                }
                /** Picture 1 */
                if(request()->picture_1 != null){
                    $picture_1 = $uploads->id.'_document'.rand(0,1000).'.'.request()->picture_1->getClientOriginalExtension();
                    Storage::disk('s3')->put($picture_1, file_get_contents($request->picture_1));
                    // $request->picture_1->storeAs('documents',$picture_1);   
                    $uploads->picture_1 = $picture_1;
                }
                /** Picture 2 */
                if(request()->picture_2 != null){
                    $picture_2 = $uploads->id.'_document'.rand(0,1000).'.'.request()->picture_2->getClientOriginalExtension();
                    Storage::disk('s3')->put($picture_2, file_get_contents($request->picture_2));
                    // $request->picture_2->storeAs('documents',$picture_2);   
                    $uploads->picture_2 = $picture_2;
                }
                /** Picture 3 */
                if(request()->picture_3 != null){
                    $picture_3 = $uploads->id.'_document'.rand(0,1000).'.'.request()->picture_3->getClientOriginalExtension();
                    Storage::disk('s3')->put($picture_3, file_get_contents($request->picture_3));
                    // $request->picture_3->storeAs('documents',$picture_3);   
                    $uploads->picture_3 = $picture_3;
                }
                /** Video */
                if(request()->video != null){
                    $video = $uploads->id.'_document'.rand(0,1000).'.'.request()->video->getClientOriginalExtension();
                    Storage::disk('s3')->put($video, file_get_contents($request->video));
                    // $request->video->storeAs('documents',$video);   
                    $uploads->video = $video;
                }
                $uploads->save();
                Session::flash('success', 'Documents updated successfully.');
            }
            return redirect('user/editdetails');
        }catch(\Exception $e){
            Session::flash('error', $e->getMessage());
            return redirect('user/editdetails');
        }
    }
}
