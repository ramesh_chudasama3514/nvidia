<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Upload;
use Auth;
use Session;
use Storage;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id','!=',Auth::id())->get();
        return view('user.list', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::where('id','=',$id)->first();
        $uploads = Upload::where('user_id', $id)->first();
        return view('user.view', ['user' => $users,'uploads' => $uploads]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'contact_no' => 'required'
        ]);
        if($request->interested_to_register == '1' && isset($request->resumefile)){
            $request->validate([
                'resume' => 'required||mimes:docx,pdf'
            ]);
        }
        try{
            $user = User::where('id',$id)->first();

            if(request()->resume != null){
                $resume = $user->id.'_resume'.rand(0,1000).'.'.request()->resume->getClientOriginalExtension();
                // $request->resume->storeAs('resume',$resume);   
                Storage::disk('s3')->put($resume, file_get_contents($request->resume));
                $user->resume = $resume;
            }
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->contact_no = $request->contact_no;
            $user->city = $request->city;
            $user->state = $request->state;
            $user->pincode = $request->pincode;
            $user->address = $request->address;
            $user->school_clg = $request->school_clg;
            $user->company_name = $request->company_name;
            $user->startup = $request->startup;
            $user->interested_to_register = $request->interested_to_register;
            $user->updated_at = now();
            $user->save();
            Session::flash('success', 'User details updated successfully.');
            return redirect()->back();
        }catch(\Exception $e){
            Session::flash('error', $e->getMessage());
            return redirect()->back()->with('error',$e->getMessage());
        }
    }
    /**
     * Update Project Details
     */
    public function updateProjectDetails(Request $request, $id){
        $upload = Upload::where('user_id',$id)->first();
        if($upload != null){
            $upload->name = $request->project_name;
            $upload->devkit = $request->devkit;
            $upload->other_accessories_used = $request->other_accessories_used;
            $upload->usecase = $request->usecase;
            $upload->otherdetails = $request->otherdetails;
            $upload->save();
        }
        Session::flash('success', 'Project details updated successfully.');
        return redirect()->back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete($id);
        Session::flash('success', 'Record has been deleted successfully!');
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }
}
