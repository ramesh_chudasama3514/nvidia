<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewfieldsInUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('uploads', function (Blueprint $table) {
            $table->string('document_file',255)->nullable()->after('otherdetails');
            $table->string('picture_1',255)->nullable()->after('document_file');
            $table->string('picture_2',255)->nullable()->after('picture_1');
            $table->string('picture_3',255)->nullable()->after('picture_2');
            $table->string('video',255)->nullable()->after('picture_3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('uploads', function (Blueprint $table) {
            //
        });
    }
}
