<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminInUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            DB::table('users')->insert([
                array(
                    'firstname' => 'Admin',
                    'lastname' => 'Admin',
                    'email' => 'info@nvidia.com',
                    'email_verified_at' => date("Y-m-d h:i:s",time()),
                    'contact_no' => '1111111111',
                    'password' => '$2y$10$TWo56r54x.7NvhMMCkIYb.HDju72uQLF/VsW9H4Yklxs2EzI6JFrq' // nvidia1234
                )
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
