@extends('layouts.master')
@section('title', ' :: Change Password')
@section('content')
<div class="profile-div">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h3 class="section-title" style="margin-top: 156px;text-align:center"> Change Password</h3>
            <form method="POST" action="{{ route('updatepassword') }}">
                @csrf
                <div class="form-group">
                    <input type="password" name="password" class="form-control-input" id="password" required>
                    <label class="label-control" for="password">{{ __('Old Password') }}<span class="required-span">*</span></label>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="password" name="new-password" class="form-control-input" id="new-password" required>
                    <label class="label-control" for="new-password">{{ __('New Password') }}<span class="required-span">*</span></label>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="password" name="new-password-confirmation" class="form-control-input" id="new-password-confirmation" required>
                    <label class="label-control" for="new-password-confirmation">{{ __('Confirm Password') }}<span class="required-span">*</span></label>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <button type="submit" class="form-control-submit-button disabled">CHANGE NOW</button>
                </div>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
@stop