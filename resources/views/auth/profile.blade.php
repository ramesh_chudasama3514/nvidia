@extends('layouts.master')
@section('title', ' :: My Profile')
@section('content')
<style>
    .document-image{
        max-width: 300px;
            border-radius: 7px;
            height: 250px;
            width: 3000px;
        }
        .content-header {
            padding-top: 135px;
        }
        .container-fluid > .row{
            margin-top: 10px;
        }
        a{
            text-decoration: none;
        }
</style>
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1></h1>
        </div>
        
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content" id="profilesection">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-11">
            </div>
             <div class="col-md-1">
                <button class="btn btn-success editprofile">Edit</button>
            </div>
         </div>
          <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Profile Information</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form action="{{route('users.update',[$user->id])}}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            @if($user->profile_pic != null)
                            <div class="col-md-2 text-center">
                                <img class="profile-user-img img-fluid img-circle" src="{{ asset('storage/app/avatars/'.$user->profile_pic) }}" alt="User profile picture">
                            </div>
                            @endif
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>First Name</label>
                                     <span class="form-control" >{{$user->firstname}}</span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Last Name </label>
                                    <span class="form-control" >{{$user->lastname}}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email </label>
                                     <span class="form-control" >{{$user->email}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Contact No </label>
                                     <span class="form-control" >{{$user->contact_no}}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>City</label>
                                     <span class="form-control">{{$user->city}}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>State</label>
                                     <span class="form-control" >{{$user->state}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Pincode</label>
                                     <span class="form-control" >{{$user->pincode}}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Education</label>
                                     <span class="form-control" >{{$user->education}}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>School/College Name</label>
                                    <span class="form-control" >{{$user->school_clg}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">Interested In Startup?</label>
                                       <span class="form-control" >{{($user->startup == 1) ? 'Yes' : 'No'}}</span>
                                      
                                    </div>
                                  </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="form-check">
                                       <label class="form-check-label">Interested to Register?</label>
                                       <span class="form-control" >{{($user->interested_to_register == 1) ? 'Yes' : 'No'}}</span>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Address</label>
                                   <span class="form-control" >{{$user->address}}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <span class="form-control" >{{$user->company_name}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                  </form>
                </div>
                <!-- /.card -->
    
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Project Details</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form action="{{route('updateproject-details',[$user->id])}}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Name </label>
                                    <span class="form-control" >{{$uploads->name}}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>NVIDIA Jetson Dev kit</label>
                                    <span class="form-control" >
                                        <?php 
                                        if(isset($uploads->devkit)){
                                            $list[1]='Nvidia Jetson nano 2Gb developer kit';
                                            $list[2]='Nvidia Jetson nano Developer kit - B01';
                                            $list[3]='Nvidia Jetson nano Developer kit - B01';
                                            $list[4]='Nvidia Jetson nano Developer kit - B01';
                                            echo $list[$uploads->devkit];
                                        }
                                        ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Other Accessories used</label>
                                    <span class="form-control" >{{$uploads->other_accessories_used}}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Application/Use case</label>
                                    <span class="form-control" >{{$uploads->usecase}}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Other details/ Reference</label>
                                    <span class="form-control" >{{$uploads->otherdetails}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                  </form>
                </div>
                <!-- /.card -->
    
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Documents</h3>
                  </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Document File </label>
                                    <div>
                                        @if(isset($uploads->document_file) && $uploads->document_file != NULL)
                                        <a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->document_file}}" target="_blank">Document File</a>
                                        @else
                                        <span>Document not uploaded.</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Picture 1</label>
                                    <div>
                                        @if(isset($uploads->picture_1) && $uploads->picture_1 != NULL)
                                        <a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_1}}" target="_blank">
                                            <img src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_1}}" alt="Image not loaded" class="document-image">
                                        </a>
                                        @else
                                        <span>Picture 1 not uploaded</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Picture 2</label>
                                    <div>
                                        @if(isset($uploads->picture_2) && $uploads->picture_2 != NULL)
                                        <a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_2}}" target="_blank">
                                            <img src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_2}}" alt="Image not loaded" class="document-image">
                                        </a>
                                         @else
                                        <span>Picture 2 not uploaded</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Picture 3</label>
                                    <div>
                                        @if(isset($uploads->picture_3) && $uploads->picture_3 != NULL)
                                        <a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_3}}" target="_blank">
                                            <img src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_3}}" alt="Image not loaded" class="document-image">
                                        </a>
                                        @else
                                        <span>Picture 3 not uploaded</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Video</label>
                                    <div>
                                        @if(isset($uploads->video) && $uploads->video != "")
                                        <iframe src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->video}}" frameborder="0"></iframe>
                                        @else
                                        <span>No Video Uploaded</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <a href="https://www.nvidia.com/en-us/gtc/?ncid=partn-47629" target="_blank">Want to know more about GTC 2021</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
    
              </div>
          </div>
      </div>
  </section>

</form>
@stop