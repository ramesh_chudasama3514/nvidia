@extends('layouts.main')
@section('content')
@include('layouts.message')
<script src="{{ asset('/public/js/jquery.dataTables.min.js') }}"></script>
<script
    src="{{ asset('/public/js/dataTables.bootstrap.min.js') }}"></script>
<link rel="stylesheet"
    href="{{ asset('/public/css/dataTables.bootstrap.min.css') }}">

    <!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Users </h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>


  <section class="content">
    @include('layouts.message')
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="card-body">
            <table class="table table-bordered" id="table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Contact</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id}}</td>
                        <td>{{ $user->firstname ." ". $user->lastname}}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->contact_no }}</td>
                        <td><a class="btn btn-success" href="{{ url('admin/users/'.$user->id.'/edit') }}">Edit</a>
                        <button class="deleteUser btn btn-primary" data-id="{{ $user->id }}" data-token="{{ csrf_token() }}" >Delete</button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

      </div>
    </div>
  </div>
  </section>

@endsection