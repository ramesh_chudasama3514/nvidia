@extends('layouts.main')
@section('content')
@include('layouts.message')
<style>
	.document-image{
		max-width: 300px;
			border-radius: 7px;
			height: 250px;
			width: 3000px;
		}
</style>
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1></h1>
        </div>
        
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">
	  <div class="container-fluid">
		  <div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="card card-primary">
				  <div class="card-header">
					<h3 class="card-title">Profile Information</h3>
				  </div>
				  <!-- /.card-header -->
				  <!-- form start -->
				  <form action="{{route('users.update',[$user->id])}}" method="POST">
					@method('PUT')
					@csrf
					<div class="card-body">
						<div class="row">
							@if($user->profile_pic != null)
							<div class="col-md-2 text-center">
								<img class="profile-user-img img-fluid img-circle" src="{{ asset('storage/app/avatars/'.$user->profile_pic) }}" alt="User profile picture">
							</div>
							@endif
							<div class="col-md-3">
								<div class="form-group">
									<label>First Name <span class="text-danger">*</span></label>
									<input type="text" name="firstname" class="form-control" placeholder="Enter first name" value="{{$user->firstname}}">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Last Name <span class="text-danger">*</span></label>
									<input type="text" name="lastname" class="form-control" placeholder="Enter last name" value="{{$user->lastname}}">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Email <span class="text-danger">*</span></label>
									<input type="email" name="email" class="form-control" placeholder="Enter email" value="{{$user->email}}" readonly>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Contact No <span class="text-danger">*</span></label>
									<input type="text" name="contact_no" class="form-control" placeholder="Enter contact number" value="{{$user->contact_no}}">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>City</label>
									<input type="text" name="city" class="form-control" placeholder="Enter city name" value="{{$user->city}}">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>State</label>
									<input type="text" name="state" class="form-control" placeholder="Enter state name" value="{{$user->state}}">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Pincode</label>
									<input type="text" name="pincode" class="form-control" placeholder="Enter pincode" value="{{$user->pincode}}">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Education</label>
									<input type="text" name="education" class="form-control" placeholder="Enter education" value="{{$user->education}}">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>School/College Name</label>
									<input type="text" name="school_clg" class="form-control" placeholder="Enter school/college name" value="{{$user->school_clg}}">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<div class="form-check">
									  <input class="form-check-input" name="is_startup" type="checkbox" {{($user->startup == 1) ? 'checked' : ''}}>
									  <label class="form-check-label">Interested In Startup?</label>
									</div>
								  </div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div class="form-check">
									  <input class="form-check-input" name="interested_to_register" type="checkbox" {{($user->interested_to_register == 1) ? 'checked' : ''}}>
									  <label class="form-check-label">Interested to Register?</label>
									</div>
								  </div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Address</label>
									<textarea name="address" class="form-control">{{$user->address}}</textarea>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Company Name</label>
									<input type="text" name="company_name" class="form-control" value="{{$user->company_name}}" />
								</div>
							</div>
						</div>
					</div>
					<!-- /.card-body -->
	
					<div class="card-footer">
					  <button type="submit" class="btn btn-primary">Update</button>
					  <a href="{{route('users.index')}}" class="btn btn-secondary">Cancel</a>
					</div>
				  </form>
				</div>
				<!-- /.card -->
	
			  </div>
		  </div>
		  <div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="card card-primary">
				  <div class="card-header">
					<h3 class="card-title">Project Details</h3>
				  </div>
				  <!-- /.card-header -->
				  <!-- form start -->
				  <form action="{{route('updateproject-details',[$user->id])}}" method="POST">
					@csrf
					<div class="card-body">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label>Name </label>
									<input type="text" name="project_name" class="form-control" placeholder="Enter project name" value="{{ isset($uploads->name) ? $uploads->name : ''}}">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>NVIDIA Jetson Dev kit</label>
									<select name="devkit" class="form-control">
										<option value="1" {{ isset($uploads->devkit) && $uploads->devkit == 1 ? 'selected' : '' }}>Nvidia Jetson nano 2Gb developer kit</option>
										<option value="2" {{ isset($uploads->devkit) && $uploads->devkit == 2 ? 'selected' : '' }}>Nvidia Jetson nano Developer kit - B01</option>
										<option value="3" {{ isset($uploads->devkit) && $uploads->devkit == 3 ? 'selected' : '' }}>Nvidia Jetson nano Developer kit - B01</option>
										<option value="4" {{ isset($uploads->devkit) && $uploads->devkit == 4 ? 'selected' : '' }}>Nvidia Jetson nano Developer kit - B01</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Other Accessories used</label>
									<textarea name="other_accessories_used" class="form-control">{{isset($uploads->other_accessories_used) ? $uploads->other_accessories_used : ''}}</textarea>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Application/Use case</label>
									<textarea name="usecase" class="form-control">{{isset($uploads->usecase) ? $uploads->usecase : ''}}</textarea>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Other details/ Reference</label>
									<textarea name="otherdetails" class="form-control">{{isset($uploads->otherdetails) ? $uploads->otherdetails : ''}}</textarea>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card-body -->
	
					<div class="card-footer">
					  <button type="submit" class="btn btn-primary">Update</button>
					  <a href="{{route('users.index')}}" class="btn btn-secondary">Cancel</a>
					</div>
				  </form>
				</div>
				<!-- /.card -->
	
			  </div>
		  </div>
		  <div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="card card-primary">
				  <div class="card-header">
					<h3 class="card-title">Documents</h3>
				  </div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-1">
								<div class="form-group">
									<label>Document File </label>
									<div>
										@if(isset($uploads->document_file))
										<a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->document_file}}" target="_blank">Document File</a>
										@endif
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Picture 1</label>
									<div>
										@if(isset($uploads->picture_1))
										<a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_1}}" target="_blank">
											<img src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_1}}" alt="Image not uploaded" class="document-image">
										</a>
										@endif
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Picture 2</label>
									<div>
										@if(isset($uploads->picture_2))
										<a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_2}}" target="_blank">
											<img src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_2}}" alt="Image not uploaded" class="document-image">
										</a>
										@endif
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Picture 3</label>
									<div>
										@if(isset($uploads->picture_3))
										<a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_3}}" target="_blank">
											<img src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->picture_3}}" alt="Image not uploaded" class="document-image">
										</a>
										@endif
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Video</label>
									<div>
										@if(isset($uploads->video) && $uploads->video != "")
										<iframe src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$uploads->video}}" frameborder="0"></iframe>
										@else
										<span>No Video Uploaded</span>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.card -->
	
			  </div>
		  </div>
	  </div>
  </section>

</form>
@endsection