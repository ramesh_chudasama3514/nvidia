@extends('layouts.master')
@section('title', ' :: My Documents')
@section('content')
<div class="my-uploads-div"><U><h3 class="text-center section-title" style="margin-top: 156px">My Documents</h3></U>
    <form method="POST" action="{{ route('updatedocuments') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group row" >
            <label for="document_file" class="col-md-4 col-form-label text-md-right">Document File<span class="required-span">*</span></label>
            <div class="col-md-6">
                <input type="file" class="form-control" name="document_file" required>
            </div>
            @if($data->document_file != null)
            <div class="col-md-2">
                <a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$data->document_file}}" target="_blank">Document File</a>
            </div>
            @endif
        </div>
        <div class="form-group row" >
            <label for="picture_1" class="col-md-4 col-form-label text-md-right">Picture 1<span class="required-span">*</span></label>
            <div class="col-md-6">
                <input type="file" class="form-control" name="picture_1" required>
            </div>
            
        </div>
        @if($data->picture_1 != null)
        <div class="form-group row">
            <div class="col-md-12 text-center">
                <a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$data->picture_1}}" target="_blank">
                    <img src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$data->picture_1}}" alt="" class="document-image">
                </a>
            </div>
        </div>
        @endif
        <div class="form-group row" >
            <label for="picture_2" class="col-md-4 col-form-label text-md-right">Picture 2</label>
            <div class="col-md-6">
                <input type="file" class="form-control" name="picture_2">
            </div>
        </div>
        @if($data->picture_2 != null)
        <div class="form-group row">
            <div class="col-md-12 text-center">
                <a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$data->picture_2}}" target="_blank">
                    <img src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$data->picture_2}}" alt="" class="document-image">
                </a>
            </div>
        </div>
        @endif
        <div class="form-group row" >
            <label for="picture_3" class="col-md-4 col-form-label text-md-right">Picture 3</label>
            <div class="col-md-6">
                <input type="file" class="form-control" name="picture_3">
            </div>
        </div>
        @if($data->picture_3 != null)
        <div class="form-group row">
            <div class="col-md-12 text-center">
                <a href="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$data->picture_3}}" target="_blank">
                    <img src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$data->picture_3}}" alt="" class="document-image">
                </a>
            </div>
        </div>
        @endif
        <div class="form-group row" >
            <label for="video" class="col-md-4 col-form-label text-md-right">Video<span class="required-span">*</span></label>
            <div class="col-md-6">
                <input type="file" class="form-control" name="video" required>
            </div>
        </div>
        @if($data->video != null)
        <div class="form-group row">
            <div class="col-md-12 text-center">
                <iframe src="{{'https://nvidia-media.s3.us-east-2.amazonaws.com/'.$data->video}}" frameborder="0"></iframe>
            </div>
        </div>
        @endif
        <div class="form-group row">
            <div class="col-md-4"></div>
            <div class="col-md-6">
                <button type="submit" class="form-control-submit-button disabled">{{ __('SAVE/UPDATE') }}</button>
            </div>
        </div>
    </form>
</div>
@stop