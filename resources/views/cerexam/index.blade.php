@extends('layouts.master')
@section('content')    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-container">
                            <h1>JETSON™ AI JEDI  </h1>
                            <h2 class="custom-h2"><span id="js-rotating"> <BR>Build it.,Submit it. , Win it.</span></h2>
                            <p class="p-heading p-large">
Develop creative projects on NVIDIA® Jetson™ Developer Kits and win prizes worth INR 25000.</p> 
                        </div>
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <!-- end of header -->


     <!-- Details 1 -->
	<div id="howto" class="accordion custom-top-margin">
		<div class="area-1">
		</div><!-- end of area-1 on same line and no space between comments to eliminate margin white space --><div class="area-2">
            
            <!-- Accordion -->
            <div class="accordion-container" id="accordionOne">
                <h2>Want to be the AI Jedi? You are just 3 simple steps away.</h2>
                <div class="item">
                    <div id="headingOne">
                        <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" role="button">
                            <span class="circle-numbering">1</span><span class="accordion-title">Get your hands on NVIDIA® Jetson™ Developer Kits</span>
                        </span>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionOne">
                        <div class="accordion-body">
                            Purchase from the below listed websites. Already have a dev kit? Great, jump to the next step.
                            <ul class="list-unstyled li-space-lg">
                            <li>
                                <a class="grey" href="https://www.tannatechbiz.com/">www.tannatechbiz.com</a>
                            </li>
                            <li>
                                <a class="grey" href="https://www.rptechindia.com/">www.rptechindia.com/</a>
                            </li>
                            
                        </ul>
                        </div>
                    </div>
                </div> <!-- end of item -->
            
                <div class="item">
                    <div id="headingTwo">
                        <span class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" role="button">
                            <span class="circle-numbering">2</span><span class="accordion-title">Register</span>
                        </span>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionOne">
                        <div class="accordion-body">
                            Our registration link will be open soon. Upload a one-page document on your project, a 2 minute video and your contact details.
                        </div>
                    </div>
                </div> <!-- end of item -->
            
                <div class="item">
                    <div id="headingThree">
                        <span class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" role="button">
                            <span class="circle-numbering">3</span><span class="accordion-title">Results</span>
                        </span>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionOne">
                        <div class="accordion-body">
                            Project submission done? Our team of experts are on the job. Stay tuned and we'll be in touch.
                        </div>
                    </div>
                </div> <!-- end of item -->
            </div> <!-- end of accordion-container -->
            <!-- end of accordion -->

		</div> <!-- end of area-2 -->
    </div> <!-- end of accordion -->
    <!-- end of details 1 -->

    <!--Rules-->
    <div id="rules" class="counter custom-rules">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="text-container">
                        <div class="section-title">RULES</div><br>
                         <ul class="list-unstyled li-space-lg first">
                            <li class="media">
                                <div class="media-bullet">1</div>
                                <div class="media-body"><strong>Eligibility </strong> <br>
                                You are eligible to participate if you meet the following requirements at time of entry:
                                <ul class="list-unstyled li-space-lg">

                                             <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">You must reside in India.</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">You are not involved in the execution or administration of this contest.</div>
                                            </li>
                                             <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">You are not an immediate family member or household member of a Tanna TechBiz/Rp Tech employee.</div>
                                            </li>
                                        </ul>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-bullet">2</div>
                                <div class="media-body"><strong>Submission Requirements</strong><br> 
                                    <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">To be eligible for the contest, you must meet ALL of the following requirements unless otherwise stated.</div>
                                            </li>
                                        </ul>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-bullet">3</div>
                                <div class="media-body"><strong>General Requirements</strong> <br>
                                     <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">You must be registered as a participant for this contest.</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Your entry must be your own original work.</div>
                                            </li>
                                           
                                        </ul>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-bullet">4</div>
                                <div class="media-body"><strong>Hardware/Software Build Requirements</strong> <br>
                                <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Your project must be created using the NVIDIA Jetson Developer Kit.</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Your project must be created using the NVIDIA JetPack SDK.</div>
                                            </li>
                                           
                                        </ul>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-bullet">5</div>
                                <div class="media-body"><strong>Project Documentation Requirements</strong> <br>
                                         <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Your project must include a project name, short project description, cover image, bill of materials (BOM), full instructions, images, and relevant resource files (schematics, code, CAD).</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Your project must be written in English.</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Your project must be submitted by date mentioned. </div>
                                            </li>
                                        </ul>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-bullet">6</div>
                                <div class="media-body"><strong>Judging criteria</strong> <br>Your project will be judged according to the rubric below.
                                        <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Project Documentation</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Detail the hardware, software and/or tools used.</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Code & Contribution</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Creativity</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Your idea doesn’t have to be original, but it should be creative.</div>
                                            </li>
                                        </ul>
                                </div>
                            </li>
                        </ul>
                        <div class="media-body"><strong>How to enter? </strong> <br></div>
                        <ul class="list-unstyled li-space-lg first">
                            <li class="media">
                                <div class="media-bullet">Step 1</div>
                                <div class="media-body"><strong>Register for the Contest </strong> <br>
                                <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Start by creating a free account on or (sign-in if already a member).</div>
                                            </li>
                                             <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Register for the contest by clicking “Register as a participant”.</div>
                                            </li>
                                        </ul>
                                </div>
                            </li>
                              <li class="media">
                                <div class="media-bullet">Step 2</div>
                                <div class="media-body"><strong>Build and Document Your Project</strong> <br>
                                <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Build your project according to the submission requirements.</div>
                                            </li>
                                             <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Build your project according to the submission requirements.</div>
                                            </li>
                                        </ul>
                                </div>
                            </li>
                              <li class="media">
                                <div class="media-bullet">Step 3</div>
                                <div class="media-body"><strong>Review and Submit Your Project </strong> <br>
                                <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Review your project and make sure it meets all the submission requirements.</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Upload a previously created project by clicking “add existing project” on the contest page.</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Submit your project by clicking “review and submit project” on the contest page.</div>
                                            </li>
                                        </ul>
                                </div>
                            </li>
                        </ul>
                        
                        <div class="media-body"><strong>Full rules </strong> <br></div>
                        <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>Common terms used in these rules</strong><br>
                                                These are the full rules that govern how the contest will operate. In these rules, “we,” “our”, and “us” refer to organizer (Tanna TechBiz and RPTech) and/or the contest host. “You” and “yourself” refer to an eligible contest participant. The full rules also include all above eligibility, requirements, and judging criteria recorded in the “rules” tab of this contest page.
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>Contest description</strong><br>
                                                This is a skill-based contest brought to you by Tanna TechBiz and RPTech and with the support by NVIDIA, on behalf of itself and its affiliates (“NVIDIA”). The object of this contest is to create project tutorials using the NVIDIA Jetson Developer Kit. For the purposes of this contest, each idea you submit will be called a “hardware application” and each project you submit in the contest will be called an “entry”. All eligible entries received will be judged using the judging criteria above to determine the final prize winners.</div>
                                            </li>
                                            <!--<li class="media">-->
                                            <!--    <i class="fas fa-square"></i>-->
                                            <!--    <div class="media-body"><strong>What are the start and end dates?</strong><br>-->
                                            <!--    Contest starts on October 3, 2019 and ends on February 14th, 2020 and will consist of the following periods:.-->
                                            <!--    •	Contest projects submission period: October 3, 2019 through February 14th, 2020.-->
                                            <!--    •	Contest winners will be announced on February 21, 2020.-->
                                            <!--    •	-->
                                            <!--    Note: Contest end dates are at the sole discretion of our and/or the contest sponsor and may be subject to change-->
                                            <!--    </div>-->
                                            <!--</li>-->
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>Can my entry be rejected?</strong><br>
                                                If your participation violates ANY of the following, you may be disqualified.
                                                •	Your entry must be your own original work.
                                                •	You must have obtained any and all consents, approvals or licenses required for you to submit your entry.
                                                •	Your entry may not include any third party trademarks (logos, names) or copyrighted materials (music, images, video, recognizable people, code, schematics etc...) unless you have obtained permission to use the materials.
                                                
                                                We reserve the right to reject an entry, in our sole and absolute discretion, that we determine contains any of the above content.
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>Important note about Copyright</strong><br>
                                                    Your entire entry must only include material (including hardware, source code – both open source and third party sourced, user interface, music, video or images) that you own or that you have permission from the copyright/trademark owner to use. Your and/or your team’s entry may not include copyrighted materials (such as source code, user interface, background music, images or video) unless you own or have permission to use the materials. Ownership is not defined as purchasing a CD at a music store for replay, if playing a copyrighted recording on your guitar or re-purposing an application’s user interface – your entry may be disqualified if copyrighted materials, including but not limited to these examples, are a part of your entry without appropriate licensing or permissions. If you do use permissible copyrighted materials, you must include the permissions information by citing the artist/creator and license information. Note that even material released under sites such as Creative Commons, common open source code licenses, and other similar licensing may need permission or acknowledgement as per the specific license. </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>Can I enter this contest as a team?</strong><br>
                                                You can enter as a team of no more than three members.
                                                    The following requirements must be satisfied to participate in this contest as a team:
                                                    •	Your team must choose one individual (the “representative”) to speak for, act, and enter a project submission on behalf of your team.
                                                    •	Each individual team member must meet ALL of the eligibility and contest requirements outlined in the full rules.
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>How will my entry be potentially used?</strong><br>
                                                    Other than what is set forth below, we are not claiming any ownership rights to your entry. However, by submitting your entry, you agree to ALL of the following statements:
                                                    •	You grant us an irrevocable, royalty-free, worldwide right and license to: (i) use, review, assess, test and otherwise analyze your entry and all its content in connection with this contest; and (ii) feature your entry and all content in connection with the marketing, sale, or promotion of this contest (including but not limited to internal and external sales meetings, conference presentations, tradeshows, and screen shots of this contest entry in press releases) in all media (now known or later developed).
                                                    •	You agree to sign any necessary documentation that may be required for us and our designees to make use of the rights you granted above.
                                                    •	You understand that after the submission deadline has passed, any project that has been submitted to the contest cannot be deleted or made private and will remain on the contest page after the contest has concluded.
                                                    •	You understand and acknowledge that the sponsor(s) may have developed or commissioned materials similar or identical to your submission and you waive any claims you may have resulting from any similarities to your entry.
                                                    •	You understand that you will not receive any compensation or credit for use of your entry, other than what is described in these full rules.
                                                    •	
                                                    Please note that following the end of this contest your entry may be posted on a website selected by us for viewing by visitors to that website. We are not responsible for any unauthorized use of your entry by visitors to this website. While we reserve these rights, we are not obligated to use your entry for any purpose, even if it has been selected as a winning entry. If you do not want to grant us these rights to your entry, please do not enter this contest.

                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>How are prizes awarded?</strong><br>
                                                    Limit one (1) prize per person or team. In the event of a tie between any eligible entries, an additional judge will break the tie based on the judging criteria described above. The decisions of the judges are final and binding. If we do not receive a sufficient number of entries meeting the entry requirements, we may, at our discretion, select fewer winners than the number of contest prizes described in the contest brief.
                                                    If you are a potential winner, we will notify you by sending a message to the e-mail address, the phone number, or mailing address (if any) provided at time of entry within seven (7) days following completion of judging. If the notification that we send is returned as undeliverable, or you are otherwise unreachable for any reason, we may award to a runner-up. If there is a dispute as to who is the potential winner, we will consider the potential winner to be the authorized account holder of the e-mail address used to enter the contest. If you do not complete the required forms as instructed and/or return the required forms within the time period listed on the winner notification message, we may disqualify you and select a runner-up.
                                                    
                                                    If you are confirmed as a winner of this contest, the following rules apply:
                                                    
                                                    •	You may not exchange your prize for cash or any other merchandise or services. However, if for any reason an advertised prize is unavailable, we reserve the right to substitute a prize of equal or greater value.
                                                    •	You may not designate someone else as the winner. If you are unable or unwilling to accept your prize, we may award it to a runner up.
                                                    •	If a prize is awarded to a project submitted by a team, the designated representative will be the sole recipient of said prize, unless otherwise agreed upon in writing by the representative and his/her team.
                                                    
                                                    Unless otherwise noted, all prizes are subject to their manufacturer’s warranty and/or terms and conditions.
                                                 </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>What other conditions am I agreeing to by entering?</strong><br>
                                                    By entering this contest you are agreeing to the following statements:
                                                            •	You agree to abide by these full rules.
                                                            •	You agree to abide by the policies outlined on our website, including our Terms of Service, Code of Conduct and Privacy Policy.
                                                            •	You agree that your personal data including your name, email address, personal address, phone number, and any other personal information disclosed during the duration of this contest may be shared with and used by Tanna TechBiz , RPTech  and NVIDIA in order to facilitate the execution of this contest.
                                                            •	You agree that Our decisions will be final and binding on all matters related to this contest.
                                                            •	You agree that, by accepting a prize, we may use your proper name and state of residence online and in print, or in any other media, in connection with this contest, without payment or compensation to you, except where prohibited by law.
    
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>What laws govern the way this contest is executed and administered?</strong><br>
                                                   This contest will be governed by the laws of the State of Gujarat, and you consent to the exclusive jurisdiction and venue of the courts of the Rajkot for any disputes arising out of this contest.
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>What if something unexpected happens and the contest cannot run as planned?</strong><br>
                                                    If someone cheats, or a virus, bug, bot, catastrophic event, or any other unforeseen or unexpected event that cannot be reasonably anticipated or controlled, (also referred to as force majeure) affects the fairness and/or integrity of this contest, we reserve the right to cancel, change or suspend this contest. This right is reserved whether the event is due to human or technical error. If a solution cannot be found to restore the integrity of the contest, we reserve the right to select winners from among all eligible entries received before we had to cancel, change or suspend the contest.

                                                    If you attempt, or we have strong reason to believe that you have compromised the integrity or the legitimate operation of this contest by cheating, hacking, creating a bot or other automated program, or by committing fraud in ANY way, we may seek damages from you to the fullest extent permitted by law. Further, we may disqualify you, and ban you from participating in any of our future contests.
                                                    Who do I contact about this contest?
                                                    If you have any questions or comments regarding this contest, please email us at aijedi@tannatechbiz.com

                                                </div>
                                            </li>
                                        </ul>
                    </div> <!-- end of text-container -->      
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of counter -->
    <!--end of Rules-->
    
     <!--Intro -->
    <div id="intro" class="basic-1 custom-basic-1 custom-top-margin">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-container">
                        <div class="section-title">Jetson Developer Kit Lineup</div>
                        <h2>Jetson developer kits are small, power-efficient AI computers, ideal for hands-on learning.</h2>
                       <div class="row">
                       <div class="col-lg-7">
                        <div class="testimonial-author">Jetson Nano 2GB Developer Kit</div>
                        <p>The NVIDIA® Jetson Nano™ 2GB Developer Kit is ideal for teaching, learning, and developing AI and robotics. With an active developer community and ready-to-build open-source projects, you’ll find all the resources you need to get started. It delivers incredible AI performance at a low price and makes the world of AI and robotics accessible to everyone with the exact same software and tools used to create breakthrough AI products across all industries. There's no better way to start.</p>
                        </div>
                        <div class="col-lg-5">
                            <div class="image-container">
                                <img class="img-fluid custom-img-fluid" src="{{ asset('public/images/intro/Tanna_Techbiz_aijedi_pages-Jetson_Nano_2GB.jpg')}}
" alt="alternative">
                            </div> 
                        </div>
                        </div>
                        <div class="row">
                       <div class="col-lg-5">
                           <div class="image-container">
                                <img class="img-fluid custom-img-fluid" src="{{ asset('public/images/intro/Tanna_Techbiz_aijedi_pages-Jetson_Nano_Devkit.jpg')}}
" alt="alternative">
                            </div>
                            </div>
                       <div class="col-lg-7">
                        <div class="testimonial-author">Jetson Nano Developer Kit</div>
                      <p>Jetson Nano Developer Kit is a small, powerful computer that lets you run multiple neural networks in parallel for applications like image classification, object detection, segmentation, and speech processing. All in an easy-to-use platform that runs in as little as 5 watts. The power of modern AI is now available for makers, learners, and embedded developers.</p>
                        </div>
                        
                        </div>
                        <div class="row">
                       <div class="col-lg-7">
                    <div class="testimonial-author">Jetson Xavier NX Developer Kit</div>

                    <p>The NVIDIA® Jetson Xavier NX™ Developer Kit brings supercomputer performance to the edge. It includes a Jetson Xavier NX module for developing multi-modal AI applications with the NVIDIA software stack in as little as 10 W. You can also now take advantage of cloud-native support to more easily develop and deploy AI software to edge devices.</p>
                    
                    <p>The developer kit is supported by the entire NVIDIA software stack, including accelerated SDKs and the latest NVIDIA tools for application development and optimization. A Jetson Xavier NX module and reference carrier board are included, along with AC power supply.</p>

                        </div>
                        <div class="col-lg-5">
                            <div class="image-container">
                                <img class="img-fluid custom-img-fluid" src="{{ asset('public/images/intro/Tanna_Techbiz_aijedi_pages-Xavier_NX_devkit.jpg')}}

" alt="alternative">
                            </div>
                        </div>
                        </div> 
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="image-container">
                                <img class="img-fluid custom-img-fluid" src="{{ asset('public/images/intro/Tanna_Techbiz_aijedi_pages-Jetson_AGX_Xavier_Devkit.jpg')}}
" alt="alternative">
                            </div>
                            </div>
                       <div class="col-lg-7">
                  <div class="testimonial-author">Jetson AGX Xavier Developer Kit</div>


                    <p>With the NVIDIA® Jetson AGX Xavier developer kit, you can easily create and deploy end-to-end AI robotics applications for manufacturing, delivery, retail, smart cities, and more. Supported by NVIDIA JetPack and DeepStream SDKs, as well as CUDA®, cuDNN, and TensorRT software libraries, the kit provides all the tools you need to get started right away. And because it’s powered by the new NVIDIA Xavier processor, you now have more than 20X the performance and 10X the energy efficiency of its predecessor, the NVIDIA® Jetson TX2. Get started today with the Jetson AGX Xavier Developer Kit.</p>
                        </div>
                        </div>
                    </div> 
                </div> 
                <!--<div class="col-lg-7">-->
                <!--    <div class="image-container">-->
                <!--        <img class="img-fluid" src="images/intro-office.jpg" alt="alternative">-->
                <!--    </div> -->
                <!--</div> -->
            </div> 
        </div> 
    </div> 
     <!--end of basic-1 -->
     <!--end of intro -->

    <!-- Description -->
    <div class="cards-1 custom-cards custom-top-margin" id="courses">
        <div class="container">
            <div class="section-title">NVIDIA COURSES</div>
            <div class="row">
                <div class="col-lg-6">
                    
                    <!-- Card -->
                    <div class="card">
                        
                        <div class="card-body">
                            <h4 class="card-title">NVIDIA® Jetson™ AI Certification Course:</h4>
                            <p>With a mission to bring AI to all classrooms,we’ve launched the NVIDIA® Jetson™ Nano 2GB Developer Kit, priced at $54 (USD). Ideal for learning, building, and teaching AI, Jetson Nano 2GB comes with easy-to-follow tutorials and ready-to-try projects.It’s the perfect tool for incorporating AI into your curriculum.We’ve also launched our Jetson AI Fundamentals Course,including four hours of free tutorials to help you get started</p>
                        </div>
                        <div class="button-container">
                            <a class="btn-solid-reg page-scroll" href="https://developer.nvidia.com/embedded/learn/jetson-ai-certification-programs#course_outline">DETAILS</a>
                        </div> <!-- end of button-container -->
                    </div>
                    <!-- end of card -->
                </div>
                <div class="col-lg-6">
                    <!-- Card -->
                    <div class="card">
                        
                        <div class="card-body">
                            <h4 class="card-title">NVIDIA® Jetson™ Nano 2GB Grant Program</h4>
                            <p>(For Professors Only)</p>
                            <p>In order to get your class AI ready, Nvidia has kickstarted the Jetson Nano 2GB grant program. If you are a professor and if you’re interested in acquiring or evaluating kits, please send over a short proposal. We’ll review your plan and work with you. If you are a student, do talk to your professor and request them to submit their short proposal</p>
                        </div>
                        <div class="button-container">
                            <a class="btn-solid-reg page-scroll" href="https://mynvidia.force.com/NvidiaGrantProgram/s/Jetson-Nano-2GB-Application">DETAILS</a>
                        </div> <!-- end of button-container -->
                    </div>
                    <!-- end of card -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of cards-1 -->
    <!-- end of description -->    

    <!-- Services -->
    <div id="services" class="cards-2 custom-top-margin">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">RESOURCES</div><br><br>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                    <!-- Card -->
                    <div class="col-lg-4">
                    <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="{{asset('public/images/custom/document.jpg')}}" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h3 class="card-title">Documentation</h3>
                            
                        </div>
                        <div class="button-container">
                            <a class="btn-solid-reg page-scroll" href="https://developer.nvidia.com/embedded/learn/getting-started-jetson#documentation">DETAILS</a>
                        </div> <!-- end of button-container -->
                    </div>
                    <!-- end of card -->
                    </div>
                    <div class="col-lg-4">

                    <!-- Card -->
                    <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="{{asset('public/images/custom/tutorial.jpg')}}" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h3 class="card-title">Tutorials</h3>
                            
                        </div>
                        <div class="button-container">
                            <a class="btn-solid-reg page-scroll" href="https://developer.nvidia.com/embedded/learn/getting-started-jetson#tutorials">DETAILS</a>
                        </div> <!-- end of button-container -->
                    </div>
                    <!-- end of card -->
                    </div>
                    <div class="col-lg-4">

                    <!-- Card -->
                    <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="{{asset('public/images/custom/videos.png')}}" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h3 class="card-title">Vidoes</h3>
                            
                        </div>
                        <div class="button-container">
                            <a class="btn-solid-reg page-scroll" href="https://www.youtube.com/channel/UCWmH73GMdI_BJBaeEpHslyQ">DETAILS</a>
                        </div> <!-- end of button-container -->
                    </div>
                    <!-- end of card -->
                    </div>
                    <div class="col-lg-4">

                    <!-- Card -->
                    <!--<div class="card">-->
                    <!--    <div class="card-image">-->
                    <!--        <img class="img-fluid" src="images/services-3.jpg" alt="alternative">-->
                    <!--    </div>-->
                    <!--    <div class="card-body">-->
                    <!--        <h3 class="card-title">courses</h3>-->
                    <!--        <p>You already are a reference point in your industry now you need to learn about acquisitions</p>-->
                    <!--        <ul class="list-unstyled li-space-lg">-->
                    <!--            <li class="media">-->
                    <!--                <i class="fas fa-square"></i>-->
                    <!--                <div class="media-body">Maintaining the leader status</div>-->
                    <!--            </li>-->
                    <!--            <li class="media">-->
                    <!--                <i class="fas fa-square"></i>-->
                    <!--                <div class="media-body">Acquisitions the right way</div>-->
                    <!--            </li>-->
                    <!--        </ul>-->
                    <!--        <p class="price">Starting at <span>$299</span></p>-->
                    <!--    </div>-->
                    <!--    <div class="button-container">-->
                    <!--        <a class="btn-solid-reg page-scroll" href="#callMe">DETAILS</a>-->
                    <!--    </div> -->
                    <!-- end of button-container -->
                    <!--</div>-->
                    <!-- end of card -->
                    </div>
                </div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container --> 
    </div> <!-- end of cards-2 -->
    <!-- end of services -->

    <!--Rules-->
    <div id="faq" class="counter custom-rules custom-faq">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="text-container">
                        <div class="section-title">FAQs</div><br>
                         
                        <ul class="list-unstyled li-space-lg">
                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>What are the steps to enter the contest?</strong><br>
                                                    <h5>Step 1: Register for the Contest</h5>
                                                    <ul>
                                                        <li>Start by creating a free account on given domain or sign-in if already a member.</li>
                                                        <li>Register for the contest by clicking “Register as a participant”.</li>
                                                    </ul>
                                                    <h5>Step 2: Build and Document Your Project</h5>
                                                    <ul>
                                                        <li>Build your project according to the submission requirements.</li>
                                                    </ul>
                                                    <h5>Step 3: Review and submit your project</h5>
                                                    <ul>
                                                        <li>Review your project and make sure it meets all the submission requirements.</li>
                                                        <li>Upload a previously created project by clicking “add existing project” on the contest page.</li>
                                                        <li>Submit your project before last date of submission by clicking “review and submit project” on the contest page.</li>
                                                    </ul>
                                                   
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>Who is eligible to participate?</strong><br>
                                                You are eligible to participate if you meet the following requirements at time of entry:
                                                <ul>
                                                    
                                                    <li>You must reside in India</li>
                                                    <li>You are not involved in the execution or administration of this contest.</li>
                                                    <li>You are not an immediate family member or household member of a Tanna TechBiz/RPTech employee.</li>
                                                </ul>
                                                
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>Did I win the contest? When will I receive my prize?</strong><br>
If you are a winner, we will contact you directly via email to arrange the shipment of your prize.                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>I submitted my project but it says "Awaiting moderation". Will it affect my eligibility?</strong><br>
                                                    As long as you submit your project before the deadline it doesn't matter when it is moderated. Just sit back and enjoy a well-deserved break!
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>My project is locked and I can't edit it anymore. Why is that?</strong><br>
                                                    All projects entered in the contest are locked after the submission deadline to ensure that they remain the same during the judging process. They are then unlocked after winners are announced.    
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>Somebody copied my entry! What can I do?</strong><br>
                                                    When judging entries, we take into account the submission date. When an entry is clearly plagiarized from another, we favor the original entry. If you believe the result of the judging is unfair, please email us at aijedi@tannatechbiz.com  to appeal. Judges decision is final.
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>What are the entry requirements?</strong><br>
                                                To be eligible for the contest, you must meet ALL of the following requirements unless otherwise stated:
                                                 <h5>General Requirements</h5>   
                                                 <ul>
                                                        <li>You must be registered as a participant for this contest.</li>
                                                        <li>Your entry must be your own original work.</li>
                                                        <li>Your team is no more than three members per entry.*</li>
                                                    </ul>
                                                    <p>*By entering a submission to this contest on behalf of a team, you acknowledge that you are the designated representative authorized to act on behalf of your team.</p>
                                                <h5>Project Submission Requirements</h5>   
                                                    <ul>
                                                        <li>You must have to register on the website.</li>
                                                        <li>You have to submit a document on the project, which must be in English.</li>
                                                        <li>Your hardware application must use the NVIDIA Jetson Developer Kit.</li>
                                                        <li>Submit photos of your project and a video as per the guidelines.</li>
	                                                    <li>To be eligible for the contest, all requirements need to be met to be eligible for the Jetson AI Jedi Contest . Any omissions will lead to disqualification. </li>

                                                        
                                                    </ul>
                                                    <p>Note :All requirements need to be met to be eligible for the Jetson AI Jedi Contest.</p>
                                                <h5>Hardware/Software Build Requirements</h5>   
                                                    <ul>
                                                        <li>Your project must be created using the NVIDIA Jetson Developer Kit.</li>
                                                        <li>Your project must be created using the NVIDIA JetPack SDK.</li>
                                                    </ul>
                                                <h5>Project Documentation Requirements</h5>   
                                                    <ul>
                                                        <li>Your project must include a project name, short project description, cover image, bill of materials (BOM), full instructions, images, and relevant resource files (schematics, code, CAD).</li>
                                                        <li>Your project must be written in English.</li>
                                                        <li>Your project must be submitted by given deadline.</li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>What are the judging criteria?</strong><br>
Your project will be judged according to the rubric below.
                                                <ul>
                                                    <li>Project Documentation </li>
                                                    <li>Story/Instructions - Show how you created your project, including images, screenshots, and/or a video demonstration of your solution working as intended. Ask yourself: “If I were a beginner reading this project, would I understand how to recreate it?”
</li>
                                                    <li>Complete BOM </li>
                                                    <li>Detail the hardware, software and/or tools used.</li>
                                                    <li>Code & Contribution</li>
                                                    <li>Include working code with helpful comments.</li>
                                                    <li>Creativity</li>
                                                    <li>Your idea doesn’t have to be original, but it should be creative (a fresh take on an old idea is often worth as much as a brand new idea)</li>
                                                    <li>Judges decision is final</li>
                                                </ul></div>
                                            </li>
                                             <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body"><strong>What are the prizes?</strong><br>
Prizes worth INR 25,000 to each of the top 10 winners
</div>
                                            </li>
                                            
                                        </ul>
                    </div> <!-- end of text-container -->      
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of counter -->
    <!--end of Rules-->

    <!-- Contact -->
    <div id="contact" class="form-2 custom-top-margin">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="text-container">
                        <div class="section-title">CONTACT</div>
                        <p>Want to reach out to us?Any concerns or queries? Just fill up the form and we'll get back to you.</p>
                        
                        
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    
                    <!-- Contact Form -->
                    <form id="contactForm" data-toggle="validator" data-focus="false">
                        <div class="form-group">
                            <input type="text" class="form-control-input" id="cname" required>
                            <label class="label-control" for="cname">Name</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control-input" id="cemail" required> 
                            <label class="label-control" for="cemail">Email</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control-textarea" id="cmessage" required></textarea>
                            <label class="label-control" for="cmessage">Your message</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="form-control-submit-button">SUBMIT MESSAGE</button>
                        </div> 
                        <div class="form-message">
                            <div id="cmsgSubmit" class="h3 text-center hidden"></div>
                        </div>
                    </form>
                    <!-- end of contact form -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of form-2 -->
    <!-- end of contact -->



    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright --> 
    <!-- end of copyright -->

@stop