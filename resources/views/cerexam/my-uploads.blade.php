@extends('layouts.master')
@section('title', ' :: My Uploads')
@section('content')
<div class="my-uploads-div">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h3 class="section-title" style="margin-top: 156px;text-align:center"> Project Details</h3>
            <form method="POST" action="{{ route('updateuploads') }}">
                @csrf
        
                <div class="form-group">
                    <input type="text" name="name" class="form-control-input notEmpty" id="name" value="{{ isset($data->name) ? $data->name : '' }}" required>
                    <label class="label-control" for="name">{{ __('Name') }}<span class="required-span">*</span></label>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <select class="form-control-input notEmpty" name="devkit" required>
                        <option value="1" {{ isset($data->devkit) && $data->devkit == 1 ? 'selected' : '' }}>Nvidia Jetson Nano 2Gb developer kit</option>
                        <option value="2" {{ isset($data->devkit) && $data->devkit == 2 ? 'selected' : '' }}>Nvidia Jetson Nano Developer kit - B01</option>
                        <option value="3" {{ isset($data->devkit) && $data->devkit == 3 ? 'selected' : '' }}>Nvidia Jetson Nano Developer kit - B01</option>
                        <option value="4" {{ isset($data->devkit) && $data->devkit == 4 ? 'selected' : '' }}>Nvidia Jetson Nano Developer kit - B01</option>
                    </select>
                    <label class="label-control" for="devkit">{{ __('NVIDIA Jetson Dev kit') }}<span class="required-span">*</span></label>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <textarea name="other_accessories_used" class="form-control-input notEmpty" id="other_accessories_used">{{ ($data != null) ? $data->other_accessories_used : "" }}</textarea>
                    <label class="label-control" for="other_accessories_used">{{ __('Other Accessories used') }}</label>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <textarea name="usecase" class="form-control-input notEmpty" id="usecase" required>{{ ($data != null) ? $data->usecase : "" }}</textarea>
                    <label class="label-control" for="usecase">{{ __('Application/Use case') }}<span class="required-span">*</span></label>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <textarea name="otherdetails" class="form-control-input notEmpty" id="otherdetails">{{ ($data != null) ? $data->otherdetails : "" }}</textarea>
                    <label class="label-control" for="otherdetails">{{ __('Other details/ Reference') }}</label>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <button type="submit" class="form-control-submit-button disabled">{{ __('SAVE/UPDATE') }}</button>
                </div>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
@stop