<!--Flash message-->
@if(Session::has('success'))
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <div class="alert-text"> {{ Session::get('success') }}</div>
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <div class="alert-text">{{ Session::get('error') }}</div>
</div>
@endif
<!--End-->
@if ($errors->any())
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif