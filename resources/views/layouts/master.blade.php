<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="">
    <meta name="author" content="Inovatik">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
	<meta property="og:site_name" content="" /> <!-- website name -->
	<meta property="og:site" content="" /> <!-- website link -->
	<meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
	<meta property="og:type" content="article" />
    <!-- Website Title -->
    <title>AIJedi @yield('title')</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="{{ asset('/public/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/public/css/fontawesome-all.css') }}" rel="stylesheet">
    <link href="{{ asset('/public/css/swiper.css') }}" rel="stylesheet">
	<link href="{{ asset('/public/css/magnific-popup.css') }}" rel="stylesheet">
	<link href="{{ asset('/public/css/styles.css') }}" rel="stylesheet">
	<link href="{{ asset('/public/css/custom.css') }}" rel="stylesheet">
<link href="{{ asset('/public/css/datepicker.css') }}" rel="stylesheet" type="text/css" />  

	<!-- Favicon  -->
    <link rel="icon" href="images/favicon.png">
    <style>
        .alert.alert-success {
            top: 112px;
            width: 100%;
            text-align: center;
        }
        .alert.alert-danger {
            top: 112px;
            width: 100%;
            text-align: center;
        }
        .btn-primary:hover {
            color: #fff;
            background-color: #113448;
            border-color: #113448;
        }
        .btn-primary {
            color: #fff;
            background-color: #14bf98;
            border-color: #14bf98;
        }
        .my-uploads-div {
            padding: 20px;
        }
        .document-image{
            max-width: 300px;
            border-radius: 7px;
        }
        .required-span{ 
            content:'*'; 
            color:red; 
            padding-left:5px;
        }
    </style>
</head>
<body data-spy="scroll" data-target=".fixed-top">
    
    <!-- Preloader -->
	<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->
    

    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top custom-navbar">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Aria</a> -->

        

        <!-- Image Logo -->
       
        <a class="navbar-brand logo-image nvidia-logo d-none d-sm-block" href="{{ url('/') }}"><img src="{{ asset('public/images/custom/nvidia_embeded.jpeg')}}" alt="nvidia" class="img-responsive"></a>
        <a class="navbar-brand logo-image nvidia-logo d-block d-sm-none" href="{{ url('/') }}"><img src="{{ asset('public/images/custom/nvidia_embeded.jpeg')}}" alt="nvidia" class="img-responsive" style="width:5.0625rem"></a>

         <table class="d-block d-sm-none">
             <tr><td><a class="navbar-brand logo-image custom-logo-image-right d-block d-sm-none" href="https://www.rptechindia.com" target="_blank">
                 <img src="{{ asset('public/images/custom/rp tech india.png')}}" alt="alternative" class="img-responsive" style="width: 5.0625rem;">        </a></td>
             <td><div class="vl"></div></td>
             <td><a class="navbar-brand logo-image custom-logo-image-right d-block d-sm-none" href="https://www.tannatechbiz.com/" target="_blank">
                 <img src="{{ asset('public/images/custom/tanna tech biz reg_logo.png')}}" alt="alternative" class="img-responsive" style="width: 5.0625rem;">        </a>

             </td>
             </table>
        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
            <span class="navbar-toggler-awesome fas fa-times"></span>
        </button>
        <!-- end of mobile menu toggle button -->

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="{{ url('/') }}">HOME <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#howto">PARTICIPATE</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#rules">RULES</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#intro">ABOUT NVIDIA® Jetson™</a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link page-scroll" href="#courses">COURSES</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#services">RESOURCES</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#faq">FAQ</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#contact">CONTACT</a>
                </li>
                @if (Auth::guest())
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link page-scroll" data-toggle="modal" 
                        data-target="#loginModal">{{ __('LOGIN') }}</a>
                </li>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link page-scroll" data-toggle="modal" 
                        data-target="#registerModal">REGISTER</a>
                </li>
                @else
                 <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        MY ACCOUNT
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="{{ url('user/myprofile') }}">My Profile</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="{{ url('user/change-password') }}">Change Password</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="{{ url('user/logout')}}">Logout</a>
                    </div>
                  </li> 
                @endif
                
            </ul>
        </div>
         
             <table class="d-none d-sm-block">
             <tr><td><a class="navbar-brand logo-image custom-logo-image-right d-none d-sm-block" href="https://www.rptechindia.com/" target="_blank">
                 <img src="{{ asset('public/images/custom/rp tech india.png')}}" alt="alternative" class="img-responsive">        </a></td>
             <td><div class="vl"></div></td>
             <td><a class="navbar-brand logo-image custom-logo-image-right d-none d-sm-block" href="https://www.tannatechbiz.com/" target="_blank"><img src="{{ asset('public/images/custom/tanna tech biz reg_logo.png')}}" alt="alternative" class="img-responsive">        </a>

             </td>
             </table>
    </nav> <!-- end of navbar -->
    <!-- end of navbar -->
        @include('layouts.message')
        @yield('content')
    <div class="form-2 custom-powered-by">
    <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="text-container">
                     <p class="p-large">Powered By</p>
                    </div>
                    </div>
                <div class="col-lg-10">
                    <a href="https://www.mistralsolutions.com/product-engineering-services/partners/nvidia/" target="_blank">
                    <span class="fa-stack custom-fa-stack">
                   <span class="hexagon custom-hexagon"></span>
                   <span class="custom-logo-span">
                               <img src="{{ asset('public/images/custom/Mistral_logo.png')}}" alt="alternative" class="img-responsive" style="width: 93px;margin-left: 22%  !important;margin-top: 6px !important;">
                    </span>
                    </span>
                    </a>
                    <a href="https://www.neoastra.com/" target="_blank">
                    <span class="fa-stack custom-fa-stack">
                   <span class="hexagon custom-hexagon"></span>
                   <span class="custom-logo-span">
                               <img src="{{ asset('public/images/custom/Neoastra.png')}}" alt="alternative" class="img-responsive" style="width: 130px;">
                    </span>
                    </span>
                    </a>
                    <a href="http://www.robothoughts.com/" target="_blank">
                    <span class="fa-stack custom-fa-stack">
                   <span class="hexagon custom-hexagon"></span>
                   <span class="custom-logo-span">
                               <img src="{{ asset('public/images/custom/robo_thoughts_logo.png')}}" alt="alternative" class="img-responsive" style="width: 120px;">
                    </span>   
                          </span>
                    </a>
                    <a href="https://smartcow.ai/en/" target="_blank">
                   <span class="fa-stack custom-fa-stack">
                   <span class="hexagon custom-hexagon"></span> 
                   <span class="custom-logo-span">
                               <img src="{{ asset('public/images/custom/smartCow.png')}}" alt="alternative" class="img-responsive" style="width: 105px;">
                    </span>
                    </span>
                    </a>
                    
                    <a href="https://www.s20.ai/" target="_blank">
                    <span class="fa-stack custom-fa-stack">
                   <span class="hexagon custom-hexagon"></span>
                   <span class="custom-logo-span">
                               <img src="{{ asset('public/images/custom/S20logo.png')}}" alt="alternative" class="img-responsive" style="width: 100px;margin-left: 38px  !important;margin-top: 6px !important;">
                    </span>
                    </span>
                    </a>
                    
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div>
    
    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="text-container about">
                        <h4>You can contact US</h4>
                        <p class="white"></p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
                <!-- end of col -->
                <div class="col-md-2">
                    <div class="text-container">
                        <h4>Contact Us</h4>
                        <ul class="list-unstyled li-space-lg">
                            <li>
                                <a class="white" href="#your-link">aijedi@tannatechbiz.com</a>
                            </li>
                            <li>
                               <a class="white" href="#your-link">contactus@rptechindia.com</a>
                            </li>
                            
                        </ul>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
                <div class="col-md-2">
                    <div class="text-container">
                        <h4>Partners</h4>
                        <ul class="list-unstyled li-space-lg">
                            <li>
                                <a class="white" href="https://www.tannatechbiz.com/">www.tannatechbiz.com</a>
                            </li>
                            <li>
                                <a class="white" href="https://www.rptechindia.com/">www.rptechindia.com/</a>
                            </li>
                            
                        </ul>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of footer -->  
    <!-- end of footer -->
    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright --> 
    <!-- end of copyright -->
    
    	
        <!-- Login modal start-->
        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title section-title" id="loginModal">{{ __('Login') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('user-login') }}">
                    @csrf
                    <div class="form-group">
                        <input type="email" name="email" class="form-control-input" id="email" required>
                        <label class="label-control" for="email">{{ __('E-Mail') }}<span class="required-span">*</span></label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control-input" id="password" required>
                        <label class="label-control" for="password">{{ __('Password') }}<span class="required-span">*</span></label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="remember" id="remember">
                        <label for="remember">{{ __('Remember Me') }}</label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="form-control-submit-button disabled">LOGIN</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Login modal end -->

<!-- Registration modal start-->
        <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registrationModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title section-title" id="registrationModal">{{ __('Registration') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('userregistration') }}">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="firstname" class="form-control-input" id="firstname" value="{{ old('firstname') }}" required>
                        <label class="label-control" for="firstname">{{ __('First Name') }}<span class="required-span">*</span></label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="lastname" class="form-control-input" id="password" value="{{ old('lastname') }}" required>
                        <label class="label-control" for="lastname">{{ __('Last Name') }}<span class="required-span">*</span></label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="contact_no" class="form-control-input" id="contact_no" required>
                        <label class="label-control" for="contact_no">{{ __('Contact Number') }}<span class="required-span">*</span></label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control-input" id="email" required>
                        <label class="label-control" for="email">{{ __('E-Mail') }}<span class="required-span">*</span></label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control-input" id="password" required>
                        <label class="label-control" for="password">{{ __('Password') }}<span class="required-span">*</span></label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password_confirmation" class="form-control-input" id="password_confirmation" required>
                        <label class="label-control" for="password_confirmation">{{ __('Confirm Password') }}<span class="required-span">*</span></label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="form-control-submit-button disabled">REGISTER NOW</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    <!-- Scripts -->
    <script src="{{ asset('/public/js/jquery.min.js') }}"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="{{ asset('/public/js/bootstrap.min.js') }}"></script> <!-- Bootstrap framework -->
    <script src="{{ asset('/public/js/popper.min.js') }}"></script> <!-- Popper tooltip library for Bootstrap -->
    
    <script src="{{ asset('/public/js/jquery.easing.min.js') }}"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="{{ asset('/public/js/swiper.min.js') }}"></script> <!-- Swiper for image and text sliders -->
    <script src="{{ asset('/public/js/jquery.magnific-popup.js') }}"></script> <!-- Magnific Popup for lightboxes -->
    <script src="{{ asset('/public/js/morphext.min.js') }}"></script> <!-- Morphtext rotating text in the header -->
    <script src="{{ asset('/public/js/isotope.pkgd.min.js') }}"></script> <!-- Isotope for filter -->
    <script src="{{ asset('/public/js/validator.min.js') }}"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="{{ asset('/public/js/scripts.js') }}"></script> <!-- Custom scripts -->
    <script src="{{ asset('/public/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/public/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript">
    $('.btn-register').on('submit', function(e){
        var registerForm = $("#registeruser");
        var formData = registerForm.serialize();

         $.ajaxSetup({
        header:$('meta[name="_token"]').attr('content')
        })
        e.preventDefault(e);
        $.ajax({
            url: $(this).attr('action'),
            type:'POST',
            data:formData,
            dataType: 'json',
            success:function(data) {
                console.log(data);
            },
            error: function(data)
            {

            }
        });
    });
    $('.datepicker').datepicker({
        format: "dd/mm/yy",
        maxDate: 0
    });
     $('.editprofile').click(function(){
        window.location.href = "<?php echo url('user/editdetails'); ?>"
     });
     // $('input[name=interested_to_register]').change(function(){
        $(document).on('change','input[name=interested_to_register]',function(){
            var value = $( 'input[name=interested_to_register]:checked' ).val();
            if(value != 0){
                $('.resume-div').css('display','block');
                $('input[name=resume]').attr('required','required');
            }else{
                $('.resume-div').css('display','none');
                $('input[name=resume]').removeAttr('required');
            }
         });
</script>
</body>
</html>