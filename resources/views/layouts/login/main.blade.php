<html lang="en">
    @include('layouts.login.head')
    <body class="login-page" style="min-height: 496.391px;">
    @yield('content')
  <!-- /.login-box -->
  
  @include('layouts.login.foot')
  
  
  </body></html>