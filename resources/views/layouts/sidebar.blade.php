<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('dashboard')}}" class="brand-link">
      {{-- <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> --}}
      <span class="brand-text font-weight-bold">Nvidia</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
			   <li class="nav-item">
				<a href="{{route('dashboard')}}" class="nav-link {{ in_array(Route::currentRouteName(), ['dashboard']) ? 'active' : ''}}">
				  <i class="nav-icon fas fa fa-tachometer-alt"></i>
				  <p>
					Dashboard
				  </p>
				</a>
			  </li>
          <li class="nav-item">
            <a href="{{route('users.index')}}" class="nav-link {{ in_array(Route::currentRouteName(), ['users.index','users.edit']) ? 'active' : ''}}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>
          </li>
          <li class="nav-item">
            <a href="{{route('admin-logout')}}" class="nav-link">
              <i class="nav-icon fas fa fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>