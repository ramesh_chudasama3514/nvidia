<!-- jQuery -->
//


<script src="{{asset('public/plugin/jquery/jquery.min.js')}}"></script>
<script src="{{asset('public/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/js/jquery.validate.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('public/plugin/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('public/js/adminlte.min.js')}}"></script>
<!-- Custom -->
<script src="{{asset('public/js/custom.js')}}"></script>
<script>
  $(document).ready(function() {
    $('#table').DataTable({
    	aoColumnDefs: [
		  {
		     bSortable: false,
		     aTargets: [ -1 ]
		  }
		]
    });
    $(".deleteUser").click(function(){
        var id = $(this).data("id");
        var token = $(this).data("token");
        if (confirm("Are sure to delete user?")) {
            $.ajax(
            {
                url: "users/"+id,
                type: 'POST',
                dataType: "JSON",
                data: {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": token,
                },
                success: function ()
                {
                    console.log("it Work");
                    window.location.reload();
                }
            });
        }
        console.log("It failed");
    });
});
 </script>