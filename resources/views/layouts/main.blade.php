<html lang="en" style="height: auto;" class="">
    @include('layouts.head')
    <body class="sidebar-mini" style="height: auto;">
        <!-- Site wrapper -->
        <div class="wrapper">
        <!-- Navbar -->
        @include('layouts.header')
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 2008.25px;">
            @yield('content')
        </div>
        <!-- /.content-wrapper -->

        @include('layouts.footer')

        <div id="sidebar-overlay"></div></div>
        <!-- ./wrapper -->
    @include('layouts.foot')
    </body>
</html>