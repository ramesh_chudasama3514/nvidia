<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/admin', 'Auth\LoginController@login')->name('admin-login');
Route::get('admin-logout', [ 'as' => 'admin-logout', 'uses' =>'Auth\LoginController@logout']);
Route::post('userregistration',  [ 'as' => 'userregistration', 'uses' =>'Auth\AuthController@storeUser']);

Route::get('/', function () {
	    return view('cerexam/index');
	});
	

Route::post('user-login', [ 'as' => 'user-login', 'uses' =>'Auth\AuthController@authenticate']);

Auth::routes();
Route::prefix('admin')->group(function () {
Route::group(array('middleware' => 'auth'), function() {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::resource('users', 'UserController');
	Route::post('users/update-project/{id}', [ 'as' => 'updateproject-details', 'uses' =>'UserController@updateProjectDetails']);
});
});
Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

Auth::routes(['verify' => true]);
Route::prefix('user')->group(function () {
	Route::group(array('middleware' => 'auth'), function() {
		Route::get('myprofile', [ 'as' => 'myprofile', 'uses' =>'User\UserController@getProfileDetails']);
		Route::get('editdetails', [ 'as' => 'editdetails', 'uses' =>'User\UserController@geteditDetailsPage']);
		Route::post('updateprofile', [ 'as' => 'updateprofile', 'uses' =>'User\UserController@updateProfileDetails']);
		Route::get('my-uploads', [ 'as' => 'my-uploads', 'uses' =>'User\UserController@getUploadDetails']);
		Route::post('updateuploads', [ 'as' => 'updateuploads', 'uses' =>'User\UserController@updateUploadDetails']);
		Route::get('my-document', [ 'as' => 'my-document', 'uses' =>'User\UserController@getMyDocuments']);
		Route::post('updatedocuments', [ 'as' => 'updatedocuments', 'uses' =>'User\UserController@updateDocuments']);
		Route::get('change-password', function () {
		    return view('auth/changepassword');
		});
		Route::post('updatepassword', [ 'as' => 'updatepassword', 'uses' =>'Auth\AuthController@changePassword']);
		Route::get('logout', 'Auth\AuthController@logout')->name('logout');
	});
});


