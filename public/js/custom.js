//const { extendWith } = require("lodash");

//const { extendWith } = require("lodash");

$(document).on("click",".user-action",function(){
    var r = confirm("Are you sure? Make sure you verified userId and Video");
    var href = $(this).attr('href');
    if (r == true) {
        window.location = href;
    }
});
$(document).on("click",".user-delete",function(){
   
    if (!confirm("Are you sure you want to delete this user")){
        return false;
      }
});
/** View Image */
$(document).on("click",".user-image",function(){
    var img = $(this).attr('src');
    $("#image-modal").modal('show');
    $("#image-modal #modal-image").attr('src',img);
});
/** View Video */

$(document).on("click",".user-video",function(){
    var url = $(this).attr('data-url');
    $("#video-modal").modal('show');
    $("#video-modal #user-video").attr('src',url);
});
$(document).on("change","#verify-chk",function(){
    if($(this).is(":checked")){
        $("#action-button").show();
    }else{
        $("#action-button").hide();
    }
});
$(document).ready( function () {
    $('#pendingTable').DataTable({
        "aLengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
        "pageLength": 5,
       
      });
      $('#InProgressTable').DataTable({
        "aLengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
        "pageLength": 5,
       
      });
      $('#ActiveTable').DataTable({
        "pageLength": 5,
        "aLengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
      });

      $('#ExpiredTable').DataTable({
        "pageLength": 5,
        "aLengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
      });

      $('#AllTable').DataTable({
        "pageLength": 5,
        "aLengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
      });
      

      $('#RejectedTable').DataTable({
        "pageLength": 5,
        "aLengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
      });

  });

 

  $(document).ready(function() {
    setFormValidation('#RegisterValidation');
   
  });


  function setFormValidation(id) {
    $(id).validate({
      highlight: function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
        $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
      },
      success: function(element) {
        $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
        $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
      },
      errorPlacement: function(error, element) {
        if(error[0].innerHTML != ''){
        error = 'Password and Confirm Password does dot match';
        }
      $(element).closest('.form-group').append(error);
      },
    });
  }
 
// $(".search-table-pending").on("keyup", function () {
//     var value = $(this).val().toLowerCase();
//     var status = 'pending'
//     $.ajax({
//         headers: {
//           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         data: {
//           id: value,
//           status: status,
//         },
//         url: 'users',
//         success: function(data) {
//           console.log(data);
//           html = '';
//           if (data != '')
//            {
//                  $.each(data, function(key, val) {
//                     $.each(val, function(key, val) {
           
//             //     if( typeof val.name !== "undefined" && val.name){ name =  val.name ;} else { name = ''} 
//             //     if( typeof val.email !== "undefined" && val.email){ email =  val.email ;} else { email = ''} 
//             //     if( typeof val.bday !== "undefined" && val.bday){ bday =  val.bday ;} else { bday = ''} 
//             //     if( typeof val.imageURL !== "undefined" && val.imageURL){ imageURL =  val.imageURL ;} else { imageURL = ''} 
//             //     if( typeof val.videoURL !== "undefined" && val.videoURL){ videoURL =  val.videoURL ;} else { videoURL = ''} 
//             //     if( typeof val.pin !== "undefined" && val.pin){ pin =  val.pin ;} else { pin = ''} 

//             // html += '<tr>' +
//             //   '<td class="text-center" >' + name + '</td>' +
//             //   '<td class="text-center" >' + email + '</td>' +
//             //   '<td class="text-center" >' + bday + '</td>' +
//             //   '<td class="text-center" >' + imageURL + '</td>' +
//             //   '<td class="text-center">' + videoURL + '</td>' +
//             //   '<td class="text-center">' + pin + '</td>' +
//             //   '</tr>';
//                  alert(val);
//                     });
             
//           });

//           $('#pending').html(html);
        
//         }
//           else{
//             $("#alertModal_id").html('NO ALERTS');}
//         },
//       });

//     // $("#myTable tbody tr").filter(function () {
//     //     $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
//     // });
// });